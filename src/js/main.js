$(document).ready(function(){

$(window).scroll(function () {
  if ($(this).scrollTop() > 0) {
      $('.to-top').fadeIn();
  } else {
      $('.to-top').fadeOut();
  }
});
$('.to-top').click(function () {
  $('body,html').animate({
      scrollTop: 0
  }, 400);
  return false;
});


//FRONT PAGE SLIDER
$('.shares-slider').slick({
  arrows: true,
  dots: true,
  speed: 600,
});

$('.tags-slider').slick({
  arrows: false,
  dots: true,
  speed: 600,
});
//--FRONT PAGE SLIDER

//PRODUCT SLIDER
$('.prod-slider').slick({
  arrows: true,
  dots: false,
  infinite: false,
  slidesToShow: 2,
  speed: 600,
});

$('.slider-main').slick({
  arrows: false,
  dots: false,
  infinite: false,
  speed: 600,
  asNavFor: '.slider-pages',
});

$('.slider-pages').slick({
  arrows: true,
  dots: false,
  infinite: false,
  speed: 600,
  slidesToShow: 4,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.slider-main',
});
//--PRODUCT SLIDER

//DROPDOWN TOP
$('.top-btn').click(function() {
  $(this).toggleClass('open');
  $('.top__menu').toggleClass('open');
  $('.t-form').removeClass('open');
});

$('.box-link').on('click touchstart', function(e) {
  e.preventDefault();
  var id = $(this).attr('href');
  var box = $(id);
  $('.t-form').removeClass('open');
  box.addClass('open');
  $('.top__menu, .top-btn').removeClass('open');
});

$('.t-form__close').on('click touchstart', function(e) {
  e.preventDefault();
  $('.t-form').removeClass('open');
});
//--DROPDOWN TOP

//BRANDS ACCORDION
$('.brand-panel').accordion({
  heightStyle: "content",
  header: '.brand-panel__title',
	icons: false,
	collapsible: true,
	active: false,
	activate: function( event, ui ) {
		if(!$.isEmptyObject(ui.newHeader.offset())) {
			$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
		}
	}
});

$('.brand-panel__title').each(function() {
  if ($(this).hasClass('ui-state-active')) {
    $(this).parent('.brand-panel__one').addClass('open');
  }
});

$('.brand-panel__title').click(function() {
  $('.brand-panel__one').removeClass('open');
  
  if ($(this).hasClass('ui-state-active')) {
    $(this).parent('.brand-panel__one').addClass('open');
  }
});
//--BRANDS ACCORDION

//PRODUCT INFO
$('.prod-info').tabs();
//--PRODUCT INFO

//REVIEW SORTING
$('.rev-sort__btn').click(function(e) {
	e.preventDefault();
	$(this).siblings('.rev-sort__box').toggleClass('open');
});
//--REVIEW SORTING

//SPOILER TEXT
$('.hidden').hide();

$('.more-text').click(function( e ) {
  $(this).parents("div").children(".hidden").slideToggle();
  e.preventDefault();
  $(this).text(function(i, text){
      return text === "Скрыть" ? "Читать полностью" : "Скрыть";
  });
});
//--SPOILER TEXT

//TOOLTIP
$('.has-tooltip').on('click touchstart', function(e) {
  $('.has-tooltip').not(this).children('.tooltip').hide();
  $(this).children('.tooltip').show();
  e.stopPropagation();
});

$(document).on('click touchstart', function(e) {
  $('.tooltip').hide();
});

//--TOOLTIP

//INPUTS
$('.input-pre').mask('(000)');
$('.input-phone').mask('000-00-00');

$('.input-pre').keyup(function () {
  if (this.value.length == this.maxLength) {
    $(this).next('input').focus();
  }
});
//--INPUTS

//VOTES
$('input[name=vote]').on('change', function() {
  var $this = $(this);
  var $parent = $this.parent();
  if ($this.is(':checked')) {
    $parent.siblings().removeClass('active');
    $parent.addClass('active').prevAll().addClass('active');
  }
});
//--VOTES
});

//PAGE CATEGORIES
$(function() {
  $("#page-categories").selectmenu();
});

$('.filter-box').accordion({
  heightStyle: "content",
  header: '.filter-box__title',
  icons: false,
  active: true,
  collapsible: true,
  beforeActivate: function(event, ui) {
    if (ui.newHeader[0]) {
        var currHeader  = ui.newHeader;
        var currContent = currHeader.next('.ui-accordion-content');
    } else {
        var currHeader  = ui.oldHeader;
        var currContent = currHeader.next('.ui-accordion-content');
    }
    var isPanelSelected = currHeader.attr('aria-selected') == 'true';

    currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

    currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

    currContent.toggleClass('accordion-content-active',!isPanelSelected)    
    if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

    return false;
  }
});

$( "#price-range" ).slider({
  range: true,
  min: 500,
  max: 30000,
  step: 500,
  values: [ 3000, 10000 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
    $( ".price-start" ).html( ui.values[ 0 ] );
    $( ".price-end" ).html( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( "$" + $( "#price-range" ).slider( "values", 0 ) +
  " - $" + $( "#price-range" ).slider( "values", 1 ) );

$('.price-start').html($( "#price-range" ).slider( "values", 0 ));
$('.price-end').html($( "#price-range" ).slider( "values", 1 ));
//--PAGE CATEGORIES

//DIALOG WINDOWS
$('.dialog-link').click(function(e) {
  e.preventDefault();
  var linkHref = $(this).attr('href');
  $(linkHref).dialog("open");
});

$('.q-order__btn').click(function(e) {
  e.preventDefault();
  $('.q-order__confirm').dialog("open");
});

$('.dialog-close').click(function(e) {
  e.preventDefault();
  $('.dialog').dialog("close");
});

$('.dialog').dialog({
  autoOpen: false,
  modal: true,
  position: {
    my: 'center',
    at: 'center',
    of: window,
  },
  show: 'fade',
  hide: 'fade',
  minWidth: 400,
  closeText: '×',
});
//--DIALOG WINDOWS

//FORM VALIDATION
$.validate({
  lang : 'ru',
});
//--FORM VALIDATION

//VIDEO POPUP
$('.video-link').magnificPopup({
  type:'iframe',
  tClose: 'Закрыть (Esc)',
});
//--VIDEO POPUP

//FAVORITE BUTTON
$('.btn-fav').click(function(e) {
	e.preventDefault();
	$(this).toggleClass('active');
})
//--FAVORITE BUTTON

//CLOSE BTN
$('.close').click(function(e) {
	e.preventDefault();
	$(this).parents('.notice').fadeOut();
})
//-/CLOSE BTN